package com.devsuperior.aulajparepository.repositories;

import com.devsuperior.aulajparepository.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

  @Query("SELECT user FROM User user WHERE user.salary >= :minSalary AND user.salary <= :maxSalary")
  Page<User> searchBySalary(Double minSalary, Double maxSalary, Pageable pageable);

  @Query("SELECT user FROM User user WHERE LOWER(user.name) LIKE LOWER(CONCAT('%',:name,'%'))")
  Page<User> searchByName(String name, Pageable pageable);

  Page<User> findBySalaryBetween(Double minSalary, Double maxSalary, Pageable pageable);

  Page<User> findByNameContainingIgnoreCase(String name, Pageable pageable);


}
